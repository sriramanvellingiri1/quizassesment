from django.db import models
from uuid import uuid4
from django.contrib.auth.models import User

# Create your models here.
class Quiz(models.Model):
    """
        Quiz Model
    """
    QUIZ_STATUS = (
        (1, 'ENABLED'),
        (0, 'DISABLED')
    )
    uuid = models.UUIDField(primary_key= True ,
                            default= uuid4 ,
                            editable = False)
    name = models.CharField(max_length=100,unique = True,error_messages={'unique':"Quiz Name already exists"})
    description = models.TextField()
    status = models.IntegerField(default=1,
                                 choices=QUIZ_STATUS)
    created_datetime = models.DateTimeField(auto_now=True)


class Question(models.Model):
    """
      Question Model
    """
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    quiz_id = models.ForeignKey(Quiz)
    created_datetime = models.DateTimeField(auto_now=True)


class QuestionChoices(models.Model):
    """
    QuestionChoices
    """
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    question_id = models.ForeignKey(Question)


class Participant(models.Model):
    """
    Particpant attending the questions
    """
    id = models.AutoField(primary_key=True)
    question_id = models.ForeignKey(Question)
    choice_id = models.ForeignKey(QuestionChoices)
    user_id = models.ForeignKey(User)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()