from django.conf.urls import url
from . import views
from rest_framework_jwt.views import obtain_jwt_token


urlpatterns = [
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^quiz', views.QuizView.as_view(), name='Get Quiz Types'),
    url(r'^question/(?P<quiz_id>.*)$', views.QuestionView.as_view(), name='Get Questions based on the Quiz'),
    url(r'^participate', views.ParticipantView.as_view(), name='Attending the Questions in the Quiz'),
]