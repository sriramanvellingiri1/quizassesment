from django.shortcuts import render
from .models import Quiz,Question,Participant
from .serializer import QuizListSerializer,QuestionListSerializer,ParticipantSerializer
from rest_framework import generics,status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# Create your views here.

class QuizView(generics.ListAPIView):
    """
    Quiz List View
    """
    serializer_class = QuizListSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        query_set = Quiz.objects.filter(status=1)
        return query_set

    def get(self,request,*args,**kwargs):
        try:
            return super(QuizView,self).list(request,*args,**kwargs)
        except Exception as e:
            return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class QuestionView(generics.ListAPIView):
    """
    Question List based on the Quiz Type
    """
    serializer_class = QuestionListSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        pass

    def get(self,request,*args,**kwargs):
        try:
            quiz_param = self.kwargs.get('quiz_id')
            query_set = Question.objects.filter(quiz_id=quiz_param)
            serializer = QuestionListSerializer(query_set, many=True)
            return Response(serializer.data,status=status.HTTP_200_OK)
        except Exception as e:
            return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ParticipantView(generics.GenericAPIView):
    """
    Particpant Quiz Submission
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ParticipantSerializer

    def post(self, request, *args, **kwargs):

        try:
            self.serializer = self.get_serializer(data=request.data)
            if not self.serializer.is_valid():
                return Response(self.serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            Participant.choice_id = request.data.get("choice_id")
            Participant.question_id = request.data.get("question_id")
            Participant.user_id = request.user.id
            Participant.start_datetime = request.data.get("start_datetime")
            Participant.end_datetime =  request.data.get("end_datetime")
            if Participant.save():
                return Response("Quiz Submitted Successfully", status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)



