from rest_framework import serializers
from .models import Quiz,Question,QuestionChoices,Participant


class QuizListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quiz
        fields = ('uuid','name','status','description')


class QuestionListSerializer(serializers.ModelSerializer):
    choices = serializers.SerializerMethodField()

    def get_choices(self,obj):
        choices = []
        if QuestionChoices.objects.filter(question_id=obj.id).exists():
            try:
                choice_list = QuestionChoices.objects.filter(question_id=obj.id)
                for options in choice_list:
                    choices.append({options.id:options.name})
            except Exception as e:
                return choices
        return choices

    class Meta:
        model = Question
        fields = ('id','name','quiz_id','choices')



class ParticipantSerializer(serializers.ModelSerializer):
    question_id = serializers.CharField(required=True)
    user_id = serializers.CharField(required=True)
    choice_id = serializers.CharField(required=True)

    class Meta:
        model = Participant
        fields = "__all__"
